import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { JwtModule } from '@nestjs/jwt'
import * as TOML from '@iarna/toml'
import * as fs from 'fs'

let TOMLSettings: any
let strTOMLPath = ''
let strTOMLSettings: string
if (fs.existsSync('settings/settings-prod.toml')) {
    strTOMLPath = 'settings/settings-prod.toml'
} else if (fs.existsSync('../settings/settings-prod.toml')) {
    strTOMLPath = '../settings/settings-prod.toml'
} else if (fs.existsSync('settings/settings.toml')) {
    strTOMLPath = 'settings/settings.toml'
} else if (fs.existsSync('../settings/settings.toml')) {
    strTOMLPath = '../settings/settings.toml'
} else {
    throw new Error("Couldn't find settings.toml file.")
}
strTOMLSettings = fs.readFileSync(strTOMLPath, 'utf8')
TOMLSettings = TOML.parse(strTOMLSettings)

@Module({
    imports: [
        JwtModule.register({
            secret: TOMLSettings.JWTSecret,
        }),
    ],
    controllers: [AppController],
})
export class AppModule {}
