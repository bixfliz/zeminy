import * as TOML from '@iarna/toml'
import { Body, Controller, Post, Req, Res } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import firebase from 'firebase'
import * as admin from 'firebase-admin'
import * as fs from 'fs'
import * as ldap from 'ldapjs'
import * as path from 'path'
import * as util from 'util'

@Controller('api')
export class AppController {
    public TOMLSettings: any
    public strMarkdownFolder: string
    public strMarkdownBackupFolder: string
    public strMarkdownDraftFolder: string
    public strMarkdownSuggestionsFolder: string

    // eslint-disable-next-line @typescript-eslint/no-parameter-properties
    public constructor(private readonly jwtService: JwtService) {
        if (!fs.existsSync('../../data')) {
            fs.mkdirSync('../../data')
        }

        let strTOMLPath = ''
        let strTOMLSettings: string
        if (fs.existsSync('settings/settings-prod.toml')) {
            strTOMLPath = 'settings/settings-prod.toml'
        } else if (fs.existsSync('../settings/settings-prod.toml')) {
            strTOMLPath = '../settings/settings-prod.toml'
        } else if (fs.existsSync('settings/settings.toml')) {
            strTOMLPath = 'settings/settings.toml'
        } else if (fs.existsSync('../settings/settings.toml')) {
            strTOMLPath = '../settings/settings.toml'
        } else {
            throw new Error("Couldn't find settings.toml file.")
        }
        strTOMLSettings = fs.readFileSync(strTOMLPath, 'utf8')
        console.log('Loaded settings from ' + strTOMLPath)
        this.TOMLSettings = TOML.parse(strTOMLSettings)
        if (this.TOMLSettings.MarkdownStorageFolder) {
            this.strMarkdownFolder = this.TOMLSettings.MarkdownStorageFolder
        } else {
            this.strMarkdownFolder = path.join(__dirname, '../../data/files')
            if (!fs.existsSync(this.strMarkdownFolder)) {
                fs.mkdirSync(this.strMarkdownFolder)
            }
        }

        if (this.TOMLSettings.MarkdownBackupStorageFolder) {
            this.strMarkdownBackupFolder = this.TOMLSettings.MarkdownBackupStorageFolder
        } else {
            this.strMarkdownBackupFolder = path.join(__dirname, '../../data/backups')
            if (!fs.existsSync(this.strMarkdownBackupFolder)) {
                fs.mkdirSync(this.strMarkdownBackupFolder)
            }
        }
        if (this.TOMLSettings.MarkdownDraftStorageFolder) {
            this.strMarkdownDraftFolder = this.TOMLSettings.MarkdownDraftStorageFolder
        } else {
            this.strMarkdownDraftFolder = path.join(__dirname, '../../data/drafts')
            if (!fs.existsSync(this.strMarkdownDraftFolder)) {
                fs.mkdirSync(this.strMarkdownDraftFolder)
            }
        }

        if (this.TOMLSettings.MarkdownSuggestionStorageFolder) {
            this.strMarkdownSuggestionsFolder = this.TOMLSettings.MarkdownSuggestionStorageFolder
        } else {
            this.strMarkdownSuggestionsFolder = path.join(__dirname, '../../data/suggestions')
            if (!fs.existsSync(this.strMarkdownSuggestionsFolder)) {
                fs.mkdirSync(this.strMarkdownSuggestionsFolder)
            }
        }

        if (!fs.existsSync(this.strMarkdownFolder)) {
            throw new Error("Error the storage folder doesn't exist: " + this.strMarkdownFolder)
        }

        if (!fs.existsSync(this.strMarkdownBackupFolder)) {
            throw new Error("Error the backup storage folder doesn't exist: " + this.strMarkdownBackupFolder)
        }
        if (!fs.existsSync(this.strMarkdownDraftFolder)) {
            throw new Error("Error the draft storage folder doesn't exist: " + this.strMarkdownDraftFolder)
        }
        if (!fs.existsSync(this.strMarkdownSuggestionsFolder)) {
            throw new Error("Error the suggestions storage folder doesn't exist: " + this.strMarkdownSuggestionsFolder)
        }
        console.log('Markdown storage folder: ' + this.strMarkdownFolder)
        console.log('Markdown backup storage folder: ' + this.strMarkdownBackupFolder)
        console.log('Markdown draft storage folder: ' + this.strMarkdownDraftFolder)
        console.log('Markdown suggestions storage folder: ' + this.strMarkdownSuggestionsFolder)

        firebase.initializeApp(this.TOMLSettings.firebaseSettings)
        admin.initializeApp({
            credential: admin.credential.cert(this.TOMLSettings.firebaseServiceAccount as admin.ServiceAccount),
            databaseURL: this.TOMLSettings.firebaseSettings.databaseURL,
        })
    }

    /*
    private encrypt(text, key) {
      let strEncryptionKey: string = key;
      if (!key){
        strEncryptionKey = this.TOMLSettings.EncryptionKey;
      }
      const iv = crypto.randomBytes(16);
      const cipher = crypto.createCipheriv('aes-256-cbc',
        Buffer.from(strEncryptionKey, 'hex'), iv);
      let encrypted = cipher.update(text);
      encrypted = Buffer.concat([encrypted, cipher.final()]);
      return `${iv.toString('hex')}:${encrypted.toString('hex')}`;
    }

    private decrypt(text, key) {
      let strEncryptionKey: string = key;
      if (!key){
        strEncryptionKey = this.TOMLSettings.EncryptionKey;
      }
      const [iv, encryptedText] = text.split(':').map(part => Buffer.from(part, 'hex'));
      const decipher = crypto.createDecipheriv('aes-256-cbc',
        Buffer.from(strEncryptionKey, 'hex'), iv);
      let decrypted = decipher.update(encryptedText);
      decrypted = Buffer.concat([decrypted, decipher.final()]);
      return decrypted.toString();
    } */

    @Post()
    public async ProcessHttpPost(
        @Body()
        PostData: {
            Action: string
            UserID: string
            Password: string
            Token: string
            Filename: string
            SuggestionFilename: string
            FileData: string
            EncryptYN: string
            EncryptionKey: string
            DeleteType: string
            GetDraftYN: string
        },
        @Req() req,
        @Res() res,
    ): Promise<any> {
        try {
            const UserSearchFilter = this.TOMLSettings.LDAPSettings.UserSearchFilter
            const UserField = this.TOMLSettings.LDAPSettings.UserField
            const URL = this.TOMLSettings.LDAPSettings.URL
            const BindAccount = this.TOMLSettings.LDAPSettings.BindAccount
            const BindAccountPassword = this.TOMLSettings.LDAPSettings.BindAccountPassword
            const jwtService = this.jwtService
            const isUserAdminYN = this.isUserAdminYN
            const adminArray = this.TOMLSettings.admin
            const ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress
            const userAgent = req.headers.userAgent

            if (PostData.Action === 'login') {
                if (this.TOMLSettings.AuthenticationType === 'LDAP') {
                    let ldapResponse = null

                    const client = ldap.createClient({
                        url: URL,
                        timeout: 5000,
                        connectTimeout: 10000,
                    })

                    const opts = {
                        filter: '(' + UserField + '=' + PostData.UserID + ')',
                        scope: 'sub',
                    }
                    try {
                        await client.bind(
                            BindAccount,
                            BindAccountPassword,
                            (error: any): void => {
                                // first need to bind
                                if (error) {
                                    console.log(error.message)
                                    client.unbind(
                                        (unbindError): void => {
                                            if (unbindError) {
                                                console.log(unbindError.message)
                                            } else {
                                                console.log('client disconnected')
                                            }
                                        },
                                    )
                                    return error;
                                }
                                client.search(
                                    UserSearchFilter,
                                    opts,
                                    (err: any, result: any): void => {
                                        result.on(
                                            'searchEntry',
                                            (entry: { raw: any }): void => {
                                                ldapResponse = entry.raw
                                            },
                                        )

                                        result.on(
                                            'end',
                                            (result: any): void => {
                                                if (!ldapResponse) {
                                                    // return res.send('Invalid username')
                                                    return res.send({
                                                        Error: 'Login error.',
                                                    })
                                                }

                                                client.bind(
                                                    ldapResponse.dn,
                                                    PostData.Password,
                                                    async (bindError: any): Promise<void> => {
                                                        // first need to bind
                                                        if (bindError) {
                                                            console.log(bindError.message)
                                                            client.unbind(
                                                                (unbindError): void => {
                                                                    if (unbindError) {
                                                                        console.log(unbindError.message)
                                                                    } else {
                                                                        console.log('client disconnected')
                                                                    }
                                                                },
                                                            )
                                                            return res.send({
                                                                Error: 'Login error.',
                                                            })
                                                        }
                                                        client.unbind(
                                                            async (unbindError): Promise<void> => {
                                                                if (unbindError) {
                                                                    console.log(unbindError.message)
                                                                }

                                                                const dtmNow: Date = new Date()
                                                                const dtmNowPlus5Hours: Date = new Date()
                                                                dtmNowPlus5Hours.setHours(dtmNow.getHours() + 5)
                                                                const numTokenExpire: number = dtmNowPlus5Hours.getTime()
                                                                const strUserIsAdminYN: string = isUserAdminYN(
                                                                    PostData.UserID,
                                                                    adminArray,
                                                                )

                                                                const cookieData = {
                                                                    User: PostData.UserID,
                                                                    Admin: strUserIsAdminYN,
                                                                    ExpireTime: numTokenExpire,
                                                                    ip: ipAddress,
                                                                    BrowserUserAgent: userAgent,
                                                                }
                                                                const responseData = {
                                                                    user: PostData.UserID,
                                                                    admin: strUserIsAdminYN,
                                                                }
                                                                res.cookie('token', await jwtService.sign(cookieData), {
                                                                    httpOnly: true,
                                                                    // secure: true,
                                                                    maxAge: 4 * 60 * 60 * 1000, // 4 hours
                                                                    sameSite: 'Strict',
                                                                })
                                                                return res.send(responseData)
                                                            },
                                                        )
                                                    },
                                                )
                                            },
                                        )
                                    },
                                )
                            },
                        )
                    } catch (error) {
                        console.log(error.message)
                        client.unbind(
                            (error): void => {
                                if (error) {
                                    console.log(error.message)
                                } else {
                                    console.log('client disconnected')
                                }
                            },
                        )

                        await this.delay(2000)
                        return res.send({
                            Error: error.message,
                        })
                    }
                } else if (this.TOMLSettings.AuthenticationType === 'Firebase') {
                    try {
                        const oFirebaseUser: firebase.auth.UserCredential = await firebase
                            .auth()
                            .signInWithEmailAndPassword(PostData.UserID, PostData.Password)
                        const strJWT = await oFirebaseUser.user.getIdToken()

                        const oFirebaseDecodedIdToken: admin.auth.DecodedIdToken = await admin
                            .auth()
                            .verifyIdToken(strJWT)

                        const strEmail: string = oFirebaseDecodedIdToken.email
                        const dtmNow: Date = new Date()
                        const dtmNowPlus5Hours: Date = new Date()
                        dtmNowPlus5Hours.setHours(dtmNow.getHours() + 48)
                        const numTokenExpire: number = dtmNowPlus5Hours.getTime()
                        const strUserIsAdminYN: string = isUserAdminYN(strEmail, adminArray)

                        if (oFirebaseUser.user.emailVerified !== true) {
                            oFirebaseUser.user.sendEmailVerification()
                            await this.delay(2000)
                            return res.send({
                                Error: 'Verification email sent. Please verify your email address.',
                            })
                        } else {
                            res.cookie(
                                'token',
                                await jwtService.sign({
                                    User: strEmail,
                                    Admin: strUserIsAdminYN,
                                    ExpireTime: numTokenExpire,
                                    ip: ipAddress,
                                    FirebaseToken: strJWT,
                                    BrowserUserAgent: userAgent,
                                }),
                                {
                                    httpOnly: true,
                                    // secure: true,
                                    maxAge: 48 * 60 * 60 * 1000, // 48 hours
                                    sameSite: 'Strict',
                                },
                            )
                            return res.send({
                                user: strEmail,
                                admin: strUserIsAdminYN,
                            })
                        }
                    } catch (error) {
                        console.log(error.message)
                        await this.delay(2000)
                        return res.send({
                            Error: error.message,
                        })
                    }
                }
            } else if (PostData.Action === 'ver') {
                return res.send({ version: this.TOMLSettings.Version })
            } else if (PostData.Action === 'read') {
                try {
                    PostData.Filename = this.protectFilepath(PostData.Filename)
                    const strReadFilePath: string = path.join(this.strMarkdownFolder, PostData.Filename + '.md')
                    const strReadDraftFilePath: string = path.join(
                        this.strMarkdownDraftFolder,
                        PostData.Filename + '.md',
                    )
                    let strReadSuggestionFilePath: string
                    let strCorrectReadFilePath: string = strReadDraftFilePath
                    let booDraftExists = false
                    let sugEmail = ''

                    if (PostData.SuggestionFilename) {
                        strReadSuggestionFilePath = path.join(
                            this.strMarkdownSuggestionsFolder,
                            PostData.SuggestionFilename + '.md',
                        )
                        sugEmail = PostData.SuggestionFilename
                        sugEmail = sugEmail.substr(sugEmail.indexOf('__-em-') + 6)
                        sugEmail = sugEmail.replace(/-at-/g, '@')
                        sugEmail = sugEmail.replace(/-dot-/g, '.')
                    }

                    if (strReadSuggestionFilePath) {
                        strCorrectReadFilePath = strReadSuggestionFilePath
                    } else if (fs.existsSync(strReadDraftFilePath)) {
                        // return a draft if it exists
                        strCorrectReadFilePath = strReadDraftFilePath
                        booDraftExists = true
                    } else {
                        strCorrectReadFilePath = strReadFilePath
                        booDraftExists = false
                    }

                    let productionData = ''
                    if (PostData.SuggestionFilename || booDraftExists) {
                        // for drafts or suggestions we want to compare the file to the production one
                        if (fs.existsSync(strReadFilePath)) {
                            productionData = fs.readFileSync(strReadFilePath, 'utf8')
                        }
                    }

                    let data = ''
                    if (fs.existsSync(strCorrectReadFilePath)) {
                        data = fs.readFileSync(strCorrectReadFilePath, 'utf8')
                    }
                    return res.send({
                        markdown: data,
                        draft: booDraftExists,
                        suggestionFile: PostData.SuggestionFilename,
                        suggestionEmail: sugEmail,
                        productionFileData: productionData,
                    })
                } catch (error) {
                    return res.send({
                        Error: error.message,
                    })
                }
            } else if (PostData.Action === 'suggest') {
                const jwtInfo = jwtService.verify(req.cookies.token)
                let strUserID = ''
                if (!jwtInfo) {
                    return res.send({
                        Error: 'Login',
                    })
                } else {
                    const dtmNow = new Date()
                    if (jwtInfo.ExpireTime < dtmNow.getTime()) {
                        return res.send({
                            Error: 'Login',
                        })
                    }
                    if (jwtInfo.ip && jwtInfo.ip !== ipAddress) {
                        return res.send({
                            Error: 'Login',
                        })
                    }
                    if (jwtInfo.BrowserUserAgent && jwtInfo.BrowserUserAgent !== userAgent) {
                        return res.send({
                            Error: 'Login',
                        })
                    }

                    /* if (jwtInfo.FirebaseToken) {
            const oFirebaseDecodedIdToken: admin.auth.DecodedIdToken = await admin
              .auth()
              .verifyIdToken(jwtInfo.FirebaseToken);
            strUserID = oFirebaseDecodedIdToken.email;
          } else {
            strUserID = jwtInfo.User;
          } */
                    strUserID = jwtInfo.User
                }
                PostData.Filename = this.protectFilepath(PostData.Filename)

                try {
                    if (!fs.existsSync(path.join(this.strMarkdownFolder, PostData.Filename + '.md'))) {
                        throw new Error("File for suggestion doesn't exits")
                    }

                    strUserID = strUserID.replace(/\@/g, '-at-')
                    strUserID = strUserID.replace(/\./g, '-dot-')
                    const strSaveFilePath: string = path.join(
                        this.strMarkdownSuggestionsFolder,
                        PostData.Filename + '__-em-' + strUserID + '.md',
                    )
                    fs.writeFileSync(strSaveFilePath, PostData.FileData)
                } catch (error) {
                    return res.send({
                        Error: error.message,
                    })
                }
            } else if (PostData.Action === 'publish' || PostData.Action === 'save-draft') {
                const jwtInfo = jwtService.verify(req.cookies.token)
                let strUserIsAdmin = ''
                let strUserID = ''

                if (!jwtInfo) {
                    return res.send({
                        Error: 'Login',
                    })
                } else {
                    const dtmNow = new Date()
                    if (jwtInfo.ExpireTime < dtmNow.getTime()) {
                        return res.send({
                            Error: 'Login',
                        })
                    }
                    if (jwtInfo.ip && jwtInfo.ip !== ipAddress) {
                        return res.send({
                            Error: 'Login',
                        })
                    }
                    if (jwtInfo.BrowserUserAgent && jwtInfo.BrowserUserAgent !== userAgent) {
                        return res.send({
                            Error: 'Login',
                        })
                    }
                    if (jwtInfo.Admin) {
                        strUserIsAdmin = 'Y'
                    }
                    /* if (jwtInfo.FirebaseToken) {
            const oFirebaseDecodedIdToken: admin.auth.DecodedIdToken = await admin
              .auth()
              .verifyIdToken(jwtInfo.FirebaseToken);
            strUserID = oFirebaseDecodedIdToken.email;
          } else {
            strUserID = jwtInfo.User;
          } */
                    strUserID = jwtInfo.User
                }
                if (!strUserIsAdmin) {
                    return res.send({
                        Error: 'Login',
                    })
                }

                PostData.Filename = this.protectFilepath(PostData.Filename)

                const strSaveFilePath: string = path.join(this.strMarkdownFolder, PostData.Filename + '.md')
                const strSaveDraftFilePath: string = path.join(this.strMarkdownDraftFolder, PostData.Filename + '.md')
                let strSaveFilePathForWrite = strSaveDraftFilePath // default
                let booFileExists = false
                let booDraftExists = false

                try {
                    booFileExists = fs.existsSync(strSaveFilePath)
                    booDraftExists = fs.existsSync(strSaveDraftFilePath)

                    if (PostData.Action === 'publish') {
                        strSaveFilePathForWrite = strSaveFilePath
                        if (booFileExists) {
                            // backup the original before we save
                            fs.copyFileSync(
                                strSaveFilePath,
                                path.join(
                                    this.strMarkdownBackupFolder,
                                    PostData.Filename + '-' + this.makeid(8) + '.md',
                                ),
                            )
                        }
                    } else if (PostData.Action === 'save-draft') {
                        strSaveFilePathForWrite = strSaveDraftFilePath
                        if (booDraftExists) {
                            // backup the original before we save
                            fs.copyFileSync(
                                strSaveDraftFilePath,
                                path.join(
                                    this.strMarkdownBackupFolder,
                                    PostData.Filename + '-draft-' + this.makeid(8) + '.md',
                                ),
                            )
                        }
                    }

                    if (booDraftExists && !booFileExists) {
                        // this makes sure that all the draft files have a corresponding file in the publish folder
                        const open = util.promisify(fs.open)
                        const close = util.promisify(fs.close)
                        // creates a 0 byte file.
                        open(strSaveFilePath, 'w').then(close)
                    }

                    // !
                    fs.writeFile(
                        strSaveFilePathForWrite,
                        PostData.FileData,
                        (error): void => {
                            if (error) {
                                return res.send({
                                    Error: error.message,
                                })
                            }

                            if (PostData.Action === 'publish') {
                                if (booDraftExists) {
                                    // backup the original before we delete it
                                    fs.copyFileSync(
                                        strSaveDraftFilePath,
                                        path.join(
                                            this.strMarkdownBackupFolder,
                                            PostData.Filename + '-draft-' + this.makeid(8) + '.md',
                                        ),
                                    )
                                    // remove draft if exists on 'publish'
                                    fs.unlink(
                                        strSaveDraftFilePath,
                                        (deleteError): void => {
                                            if (deleteError) {
                                                throw deleteError
                                            }
                                            // if no error, file has been deleted successfully
                                        },
                                    )
                                }
                            }

                            this.cleanUpBackupFolder()
                            return res.send({
                                Success: 'Saved file: ' + strSaveFilePathForWrite,
                            })
                        },
                    )
                } catch (error) {
                    return res.send({
                        Error: error.message,
                    })
                }
            } else if (PostData.Action === 'delete-list') {
                PostData.Filename = this.protectFilepath(PostData.Filename)

                const strDeleteFilePath: string = path.join(this.strMarkdownFolder, PostData.Filename + '.md')
                const strDeleteDraftFilePath: string = path.join(this.strMarkdownDraftFolder, PostData.Filename + '.md')
                let booFileExists = false
                let booDraftExists = false
                const arrSuggestionEmails: {
                    filename: string
                    sugfilename: string
                    email: string
                }[] = []

                try {
                    booFileExists = fs.existsSync(strDeleteFilePath)
                    booDraftExists = fs.existsSync(strDeleteDraftFilePath)

                    const suggestionFiles = fs
                        .readdirSync(this.strMarkdownSuggestionsFolder)
                        .filter((fn): boolean => fn.startsWith(PostData.Filename + '__-em-'))
                    for (const suggestionFile of suggestionFiles) {
                        let sugEmail = ''
                        if (suggestionFile.indexOf('__-em-') > -1) {
                            sugEmail = suggestionFile
                            sugEmail = sugEmail.substr(sugEmail.indexOf('__-em-') + 6)
                            sugEmail = sugEmail.replace(/-at-/g, '@')
                            sugEmail = sugEmail.replace(/-dot-/g, '.')
                            sugEmail = sugEmail.replace(/\.md$/i, '')
                            const suggestionFileWithoutExtension: string = suggestionFile.replace(/\.md$/i, '')
                            arrSuggestionEmails.push({
                                filename: PostData.Filename,
                                sugfilename: suggestionFileWithoutExtension,
                                email: sugEmail,
                            })
                        }
                    }
                    return res.send({
                        productionFileExists: booFileExists,
                        draftFileExists: booDraftExists,
                        suggestions: arrSuggestionEmails,
                    })
                } catch (error) {
                    return res.send({
                        Error: error.message,
                    })
                }
            } else if (PostData.Action === 'delete') {
                try {
                    const jwtInfo = jwtService.verify(req.cookies.token)
                    let strUserIsAdmin = ''

                    if (!jwtInfo) {
                        return res.send({
                            Error: 'Login',
                        })
                    } else {
                        const dtmNow = new Date()
                        if (jwtInfo.ExpireTime < dtmNow.getTime()) {
                            return res.send({
                                Error: 'Login',
                            })
                        }
                        if (jwtInfo.ip && jwtInfo.ip !== ipAddress) {
                            return res.send({
                                Error: 'Login',
                            })
                        }
                        if (jwtInfo.BrowserUserAgent && jwtInfo.BrowserUserAgent !== userAgent) {
                            return res.send({
                                Error: 'Login',
                            })
                        }
                        if (jwtInfo.Admin) {
                            strUserIsAdmin = 'Y'
                        }
                        /*             if (jwtInfo.FirebaseToken) {
                          const oFirebaseDecodedIdToken: admin.auth.DecodedIdToken = await admin
                            .auth()
                            .verifyIdToken(jwtInfo.FirebaseToken);
                        } */
                    }
                    if (!strUserIsAdmin) {
                        return res.send({
                            Error: 'Login',
                        })
                    }

                    PostData.Filename = this.protectFilepath(PostData.Filename)

                    if (PostData.DeleteType === 'p') {
                        fs.unlink(
                            path.join(this.strMarkdownFolder, PostData.Filename + '.md'),
                            (error): void => {
                                if (error) {
                                    throw error
                                }
                            },
                        )
                    } else if (PostData.DeleteType === 'd') {
                        fs.unlink(
                            path.join(this.strMarkdownDraftFolder, PostData.Filename + '.md'),
                            (error): void => {
                                if (error) {
                                    throw error
                                }
                            },
                        )
                    } else if (PostData.DeleteType === 's') {
                        fs.unlink(
                            path.join(this.strMarkdownSuggestionsFolder, PostData.Filename + '.md'),
                            (error): void => {
                                if (error) {
                                    throw error
                                }
                            },
                        )
                    }

                    const draftFiles = fs.readdirSync(this.strMarkdownDraftFolder)
                    const suggestionFiles = fs.readdirSync(this.strMarkdownSuggestionsFolder)

                    let strProductionPath = ''
                    let strProductionName = ''

                    for (const suggestionFile of suggestionFiles) {
                        // this makes sure that all the suggestion files have a corresponding file in the publish folder
                        const open = util.promisify(fs.open)
                        const close = util.promisify(fs.close)
                        // create a 0 byte file.
                        strProductionName = suggestionFile.substring(0, suggestionFile.indexOf('__-em-')) + '.md'
                        strProductionPath = path.join(this.strMarkdownFolder, strProductionName)
                        if (!fs.existsSync(strProductionPath)) {
                            open(strProductionPath, 'w').then(close)
                        }
                    }
                    for (const draftFile of draftFiles) {
                        // this makes sure that all the draft files have a corresponding file in the publish folder
                        const open = util.promisify(fs.open)
                        const close = util.promisify(fs.close)
                        // create a 0 byte file.
                        strProductionPath = path.join(this.strMarkdownFolder, draftFile)
                        if (!fs.existsSync(strProductionPath)) {
                            open(strProductionPath, 'w').then(close)
                        }
                    }
                } catch (error) {
                    return res.send({
                        Error: error.message,
                    })
                }
            } else if (PostData.Action === 'logout') {
                const oFirebaseDecodedIdToken: admin.auth.DecodedIdToken = await admin
                    .auth()
                    .verifyIdToken(PostData.Token)
                firebase.auth().signOut()
                res.cookie('token', {
                    httpOnly: true,
                    // secure: true,
                    maxAge: Date.now(),
                    sameSite: 'Strict',
                })

                return res.send('')
            } else if (PostData.Action === 'list') {
                let strFileWithoutExtension = ''
                const arrFilesTitles: {
                    name: string
                    draft: boolean
                    suggestions: {
                        filename: string
                        sugfilename: string
                        email: string
                    }[]
                }[] = []
                let booDraftExists = false
                let arrSuggestionEmails: {
                    filename: string
                    sugfilename: string
                    email: string
                }[] = []
                const productionFiles = fs.readdirSync(this.strMarkdownFolder)
                const draftFiles = fs.readdirSync(this.strMarkdownDraftFolder)
                const suggestionFiles = fs.readdirSync(this.strMarkdownSuggestionsFolder)

                let strProductionPath = ''
                let strProductionName = ''

                for (const suggestionFile of suggestionFiles) {
                    // this makes sure that all the draft files have a corresponding file in the publish folder
                    const open = util.promisify(fs.open)
                    const close = util.promisify(fs.close)
                    // creates a 0 byte file.
                    strProductionName = suggestionFile.substring(0, suggestionFile.indexOf('__-em-')) + '.md'
                    strProductionPath = path.join(this.strMarkdownFolder, strProductionName)
                    if (!fs.existsSync(strProductionPath)) {
                        open(strProductionPath, 'w').then(close)
                    }
                }
                for (const draftFile of draftFiles) {
                    // this makes sure that all the draft files have a corresponding file in the publish folder
                    const open = util.promisify(fs.open)
                    const close = util.promisify(fs.close)
                    // creates a 0 byte file.
                    strProductionPath = path.join(this.strMarkdownFolder, draftFile)
                    if (!fs.existsSync(strProductionPath)) {
                        open(strProductionPath, 'w').then(close)
                    }
                }
                for (const productionFile of productionFiles) {
                    if (productionFile.endsWith('.md')) {
                        strFileWithoutExtension = productionFile.replace(/\.md$/i, '')
                        arrSuggestionEmails = []
                        const suggestionFiles = fs
                            .readdirSync(this.strMarkdownSuggestionsFolder)
                            .filter(fn => fn.startsWith(strFileWithoutExtension + '__-em-'))
                        for (const suggestionFile of suggestionFiles) {
                            let sugEmail = ''
                            if (suggestionFile.indexOf('__-em-') > -1) {
                                sugEmail = suggestionFile
                                sugEmail = sugEmail.substr(sugEmail.indexOf('__-em-') + 6)
                                sugEmail = sugEmail.replace(/-at-/g, '@')
                                sugEmail = sugEmail.replace(/-dot-/g, '.')
                                sugEmail = sugEmail.replace(/\.md$/i, '')
                                const suggestionFileWithoutExtension: string = suggestionFile.replace(/\.md$/i, '')
                                arrSuggestionEmails.push({
                                    filename: strFileWithoutExtension,
                                    sugfilename: suggestionFileWithoutExtension,
                                    email: sugEmail,
                                })
                            }
                        }
                        booDraftExists = fs.existsSync(path.join(this.strMarkdownDraftFolder, productionFile))
                        arrFilesTitles.push({
                            name: strFileWithoutExtension,
                            draft: booDraftExists,
                            suggestions: arrSuggestionEmails,
                        })
                    }
                }
                return res.send(arrFilesTitles)
            } else {
                await this.delay(2000)
                return res.send({
                    Error: 'Invalid',
                })
            }
        } catch (error) {
            console.log(error.message)
            await this.delay(2000)
            return res.send({
                Error: error.message,
            })
        }
    }

    private isUserAdminYN(user: string, adminArray: any): string {
        let strUserIsAdminYN = 'N'
        if (adminArray && adminArray instanceof Array) {
            for (const adminEmail of adminArray) {
                if (typeof adminEmail.id === 'string') {
                    if (adminEmail.id.toLowerCase() === user.toLowerCase()) {
                        strUserIsAdminYN = 'Y'
                        return strUserIsAdminYN
                        break
                    }
                }
            }
        }
        return strUserIsAdminYN
    }

    private protectFilepath(filepath: string): string {
        let strReturn: string = filepath
        strReturn = strReturn.replace(/\./g, '-')
        strReturn = strReturn.replace(/ /g, '_')
        strReturn = strReturn.replace(/\//g, '-')
        strReturn = strReturn.replace(/\\/g, '-')
        strReturn = strReturn.replace(/~/g, '-')
        return strReturn
    }

    private makeid(length): string {
        let result = ''
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
        const charactersLength = characters.length
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength))
        }
        return result
    }
    private delay(ms: number): Promise<void> {
        return new Promise((resolve): NodeJS.Timeout => setTimeout(resolve, ms))
    }
    private cleanUpBackupFolder(): void {
        const files = fs.readdirSync(this.strMarkdownBackupFolder)
        const maxBackups: number = this.TOMLSettings.MaxBackups
        const backupFolder: string = this.strMarkdownBackupFolder
        files.sort(
            (a, b): any => {
                return (
                    fs.statSync(path.join(backupFolder, b)).mtime.getTime() -
                    fs.statSync(path.join(backupFolder, a)).mtime.getTime()
                )
            },
        )

        let strFileToDelete = ''
        files.forEach(
            (file, index): void => {
                // console.log('index: ' + index + ' ' + file + ' ');
                // console.dir(fs.statSync(path.join(backupFolder, file)).mtime);
                if (index > maxBackups) {
                    strFileToDelete = path.join(this.strMarkdownBackupFolder, file)
                    fs.unlinkSync(strFileToDelete)
                    console.log('Deleted file: ' + strFileToDelete)
                }
            },
        )
    }
}
