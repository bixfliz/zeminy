import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import * as TOML from '@iarna/toml'
import * as fs from 'fs'
import * as cookieParser from 'cookie-parser'
import * as path from 'path'

async function bootstrap(): Promise<void> {
    const app = await NestFactory.create(AppModule)
    app.use(cookieParser())

    app.enableCors()
    let TOMLSettings: any
    let strTOMLPath = ''
    let strTOMLSettings: string
    if (fs.existsSync('settings/settings-prod.toml')) {
        strTOMLPath = 'settings/settings-prod.toml'
    } else if (fs.existsSync('../settings/settings-prod.toml')) {
        strTOMLPath = '../settings/settings-prod.toml'
    } else if (fs.existsSync('settings/settings.toml')) {
        strTOMLPath = 'settings/settings.toml'
    } else if (fs.existsSync('../settings/settings.toml')) {
        strTOMLPath = '../settings/settings.toml'
    } else {
        throw new Error("Couldn't find settings.toml file.")
    }
    strTOMLSettings = fs.readFileSync(strTOMLPath, 'utf8')
    console.log('Loaded settings from ' + strTOMLPath)
    TOMLSettings = TOML.parse(strTOMLSettings)
    app.useStaticAssets(path.join(__dirname, '../../frontend/dist'))

    await app.listen(TOMLSettings.ServerPort)
}
bootstrap()
