todo:
  - edit draft and production independently
  - make firebase tokens last longer
  - email admins about suggestions - SMTP
  - error: draft contains no changes from the production file
    - suggestions too
  - show when unsaved (with an asterisk) - noticeable
  - ctrl-s to save ?
  - create docker image for docker hub 
