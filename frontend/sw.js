



/**
 * Check out https://googlechromelabs.github.io/sw-toolbox/ for
 * more info on how to use sw-toolbox to custom configure your service worker.
 */

'use strict';
importScripts('./sw-toolbox.js');

self.toolbox.options.cache = {
	name: 'zeminy-2'
};

// pre-cache our key assets
self.toolbox.precache([
  'index.html',
  'toast.js',
  'manifest.json',
  'sw-toolbox.js',
  'sw.js',
  'zeminy.js',
  'settings/settings.js',
  'favicon/pwa-icon-128x128.png',
  'favicon/apple-icon-76x76.png',
  'favicon/apple-icon-120x120.png',
  'favicon/ms-icon-310x310.png',
  'favicon/favicon.ico',
  'favicon/apple-icon.png',
  'favicon/favicon-16x16.png',
  'favicon/apple-icon-72x72.png',
  'favicon/apple-icon-114x114.png',
  'favicon/android-icon-72x72.png',
  'favicon/android-icon-96x96.png',
  'favicon/apple-icon-57x57.png',
  'favicon/pwa-icon-256x256.png',
  'favicon/apple-icon-precomposed.png',
  'favicon/manifest.json',
  'favicon/ms-icon-70x70.png',
  'favicon/browserconfig.xml',
  'favicon/android-icon-144x144.png',
  'favicon/android-icon-192x192.png',
  'favicon/ms-icon-150x150.png',
  'favicon/ms-icon-144x144.png',
  'favicon/android-icon-36x36.png',
  'favicon/apple-icon-180x180.png',
  'favicon/apple-icon-60x60.png',
  'favicon/apple-icon-144x144.png',
  'favicon/favicon-96x96.png',
  'favicon/android-icon-48x48.png',
  'favicon/apple-icon-152x152.png',
  'favicon/favicon-32x32.png',
]);

// dynamically cache any other local assets
self.toolbox.router.any('/*', self.toolbox.networkFirst);

// for any other requests go to the network, cache,
// and then only use that cached resource if your user goes offline
self.toolbox.router.default = self.toolbox.networkFirst;
