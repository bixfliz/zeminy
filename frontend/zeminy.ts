declare var CryptoJS: any
declare var SimpleMDE: any
declare var bootbox: any
declare var Vue: any
declare var $: any
declare var iqwerty: any
declare var Diff: any
declare var zeminySettings: any
let settings = JSON.parse(zeminySettings)

const strVersion = '0.0.9'
const strPageTitle = settings.PageTitle
const strAPI = settings.apiUrl
// const strAPI = 'api/main';
const strFooterHTML = settings.FooterHtml
const booEnableEncryption = settings.EnableEncryption
const booAlwaysShowMenu = settings.AlwaysShowMenu
let vue: any
let strSavedEncryptionKey = ''

const strProblemIconHTML = '<i class="fa fa-exclamation-triangle text-danger" aria-hidden="true"></i> '

function setCookie(cname: string, cvalue: string, exdays: number): void {
    const cookieExpireDate = new Date()
    cookieExpireDate.setTime(cookieExpireDate.getTime() + exdays * 24 * 60 * 60 * 1000)
    const expires = 'expires=' + cookieExpireDate.toUTCString()
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/'
}

function getCookie(cname: string): string {
    const name = cname + '='
    const decodedCookie = decodeURIComponent(document.cookie)
    const ca = decodedCookie.split(';')
    for (let c of ca) {
        // c is for cookie and that's good enough for me
        while (c.charAt(0) === ' ') {
            c = c.substring(1)
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length)
        }
    }
    return ''
}

function showToastMessage(message: string, milliseconds: number): void {
    const options = {
        settings: {
            duration: milliseconds,
        },
    }

    const msgSpan = document.createElement('span')
    msgSpan.innerHTML = message

    iqwerty.toast.Toast(msgSpan, options)
}

function setLoginState(IsLoggedIn: boolean): void {
    if (!vue.username) {
        vue.username = getCookie('user')
    }
    vue.loggedIn = IsLoggedIn
    if (!IsLoggedIn) {
        setCookie('token', '', 4)
        setCookie('admin', '', 4)
        setCookie('user', '', 4)
    }
}

function encrypt(): void {
    const ciphertext = CryptoJS.AES.encrypt(vue.simplemde.value(), strSavedEncryptionKey)
    vue.simplemde.value('ENCAES256' + ciphertext.toString())
}

function decrypt(): void {
    const bytes = CryptoJS.AES.decrypt(vue.simplemde.value().substring(9), strSavedEncryptionKey)
    const result = bytes.toString(CryptoJS.enc.Utf8)
    if (result) {
        vue.simplemde.value(bytes.toString(CryptoJS.enc.Utf8))
    }
}

function protectFilepath(filepath: string): string {
    let strReturn = filepath
    strReturn = strReturn.replace(/\./g, '-')
    strReturn = strReturn.replace(/ /g, '_')
    strReturn = strReturn.replace(/\//g, '-')
    strReturn = strReturn.replace(/\\/g, '-')
    strReturn = strReturn.replace(/~/g, '-')
    strReturn = strReturn.replace(/__/g, '_')

    return strReturn
}

vue = new Vue({
    el: '#app',
    data: {
        appVersion: strVersion,
        loggedIn: false,
        pageTitleLabel: strPageTitle,
        currentFilename: '',
        currentFileOriginalText: '',
        fileList: [],
        fileSearch: '',
        username: '',
        password: '',
        footerHTML: strFooterHTML,
        suggestionEmail: '',
        simplemde: {},
        enableEncryption: booEnableEncryption,
        isDraft: false,
        admin: false,
        alwaysShowMenu: booAlwaysShowMenu,
    },
    methods: {
        getFileText(fileToOpen: string, suggestionFile: string, cb: any): void {
            vue.currentFilename = fileToOpen

            const oAPIPostRequest = new XMLHttpRequest()
            oAPIPostRequest.onreadystatechange = (): void => {
                try {
                    if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
                        let objResponse: any
                        if (oAPIPostRequest.responseText) {
                            objResponse = JSON.parse(oAPIPostRequest.responseText)
                        }
                        if (objResponse && objResponse.Error) {
                            showToastMessage('Error: ' + objResponse.Error, 2500)
                            if (typeof cb === 'function') {
                                cb({})
                            }
                        } else if (objResponse) {
                            if (typeof cb === 'function') {
                                cb(objResponse)
                            }
                        }
                    }
                } catch (err) {
                    cb({})
                }
            }
            oAPIPostRequest.open('POST', strAPI)
            oAPIPostRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
            if (!suggestionFile) {
                oAPIPostRequest.send('Action=read&' + 'Filename=' + encodeURIComponent(fileToOpen))
            } else {
                oAPIPostRequest.send(
                    'Action=read&' +
                        'Filename=' +
                        encodeURIComponent(fileToOpen) +
                        '&' +
                        'SuggestionFilename=' +
                        encodeURIComponent(suggestionFile),
                )
            }
        },
        loadFile(fileToOpen: string, suggestionFile: string): void {
            vue.currentFilename = fileToOpen
            vue.showDeleteButton = false
            vue.getFileText(
                fileToOpen,
                suggestionFile,
                (response: {
                    draft: boolean
                    markdown: string
                    suggestionFile: string
                    suggestionEmail: string
                    productionFileData: string
                }): void => {
                    if (response.draft) {
                        vue.isDraft = true
                    } else {
                        vue.isDraft = false
                    }
                    if (response.suggestionEmail) {
                        vue.suggestionEmail = response.suggestionEmail
                    }
                    if (!suggestionFile) {
                        vue.currentFileOriginalText = response.markdown
                    } else {
                        vue.currentFileOriginalText = ''
                    }
                    vue.simplemde.value(response.markdown)

                    if (response.productionFileData && response.productionFileData !== response.markdown) {
                        let color = ''
                        let span = null
                        const diff = Diff.diffWords(response.productionFileData, response.markdown)
                        const div = document.createElement('div')

                        diff.forEach(
                            (part: { added: boolean; removed: boolean; value: string }): void => {
                                // green for additions, red for deletions
                                // grey for common parts
                                color = part.added ? 'green' : part.removed ? 'red' : 'grey'
                                span = document.createElement('span')
                                span.style.color = color
                                span.appendChild(document.createTextNode(part.value))
                                div.appendChild(span)
                            },
                        )

                        if (suggestionFile) {
                            bootbox
                                .alert({
                                    message:
                                        '<h4>This change suggestion for the file <b>' +
                                        vue.currentFilename +
                                        `</b> contains the following differences from the file in production:</h4><br><pre>` +
                                        div.innerHTML +
                                        '</pre>',
                                    size: 'large',
                                })
                                .find('div.modal-dialog')
                                .addClass('largeBootBoxWidth')
                        } else {
                            bootbox
                                .alert({
                                    message:
                                        '<h4>This draft for the file <b>' +
                                        vue.currentFilename +
                                        `</b> contains the following differences from the file in production:</h4><br><pre>` +
                                        div.innerHTML +
                                        '</pre>',
                                    size: 'large',
                                })
                                .find('div.modal-dialog')
                                .addClass('largeBootBoxWidth')
                        }
                    }
                },
            )
        },
        pageLoded(): void {
            vue.simplemde = new SimpleMDE({
                element: document.getElementById('txtSimplemde'),
                renderingConfig: {
                    singleLineBreaks: false,
                    codeSyntaxHighlighting: true,
                },
                previewRender(plainText: string): void {
                    let textModified = plainText
                    textModified = textModified.replace(/\</g, '&lt;')
                    textModified = textModified.replace(/\>/g, '&gt;')
                    return vue.simplemde.markdown(textModified) // Returns HTML from a custom parser
                },
            })

            document.title = strPageTitle
            vue.appVersion = strPageTitle + ': ' + strVersion

            if (!vue.username) {
                vue.username = getCookie('user')
            }
            vue.admin = getCookie('admin') === 'Y'
            if (vue.username) {
                setLoginState(true)
            }

            const oURLQueryString = new URLSearchParams(window.location.search)

            if (oURLQueryString.has('f') && oURLQueryString.has('s')) {
                vue.loadFile(oURLQueryString.get('f'), oURLQueryString.get('s'))
            } else if (oURLQueryString.has('f')) {
                vue.loadFile(oURLQueryString.get('f'))
            }
            const el = document // This can be your element on which to trigger the event
            const event = document.createEvent('HTMLEvents')
            event.initEvent('resize', true, false)
            el.dispatchEvent(event)
        },
        changeFilename(): void {
            bootbox.prompt({
                title: 'Set the name of the file.',
                backdrop: true,
                value: vue.currentFilename,
                callback(result: any): void {
                    if (result !== null) {
                        vue.currentFilename = protectFilepath(result)
                    }
                },
            })
        },
        listFiles(): void {
            const oAPIPostRequest = new XMLHttpRequest()
            oAPIPostRequest.onreadystatechange = (): void => {
                if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
                    vue.fileSearch = ''

                    let objResponse: any
                    if (oAPIPostRequest.responseText) {
                        objResponse = JSON.parse(oAPIPostRequest.responseText)
                    } else {
                        objResponse = null
                    }
                    vue.fileList = objResponse
                    $('.navbar-nav>li>a').on('click', () => {
                        $('.navbar-collapse').collapse('hide')
                    })
                }
            }
            oAPIPostRequest.open('POST', strAPI)
            oAPIPostRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
            oAPIPostRequest.send('Action=list&')
        },
        logout(): void {
            if (!vue.username) {
                vue.username = getCookie('user')
            }
            vue.admin = getCookie('admin') === 'Y'

            if (!vue.username) {
                bootbox.alert(strProblemIconHTML + 'Please login first.')
                return
            }

            const oAPIPostRequest = new XMLHttpRequest()
            oAPIPostRequest.onreadystatechange = (): void => {
                if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
                    setLoginState(false)
                    $('.navbar-nav>li>a').on('click', () => {
                        $('.navbar-collapse').collapse('hide')
                    })
                }
            }
            oAPIPostRequest.open('POST', strAPI)
            oAPIPostRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
            oAPIPostRequest.send('Action=logout')
        },
        login(event: any): void {
            setLoginState(false)

            const oAPIPostRequest = new XMLHttpRequest()
            oAPIPostRequest.onreadystatechange = (): void => {
                if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
                    let objResponse: any
                    if (oAPIPostRequest.responseText) {
                        objResponse = JSON.parse(oAPIPostRequest.responseText)
                    } else {
                        objResponse = null
                    }
                    if (oAPIPostRequest.responseText && objResponse.user) {
                        // user is logged in
                        vue.username = objResponse.user
                        vue.admin = objResponse.admin
                        setCookie('admin', vue.admin, 4)
                        setCookie('user', vue.username, 4)
                        setLoginState(true)
                        $('#modalLoginForm').modal('hide')
                    } else {
                        setLoginState(false)
                        let strError = ''
                        if (objResponse.Error) {
                            strError = ' ' + objResponse.Error
                        }
                        showToastMessage(strProblemIconHTML + 'Login failed.' + strError, 2500)
                    }
                    $('.navbar-nav>li>a').on('click', () => {
                        $('.navbar-collapse').collapse('hide')
                    })
                }
            }
            oAPIPostRequest.open('POST', strAPI)
            oAPIPostRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
            oAPIPostRequest.send(
                'Action=login&' +
                    'UserID=' +
                    encodeURIComponent(vue.username) +
                    '&' +
                    'Password=' +
                    encodeURIComponent(vue.password),
            )
        },
        deleteFile(): void {
            if (!vue.username) {
                vue.username = getCookie('user')
            }
            vue.admin = getCookie('admin') === 'Y'
            if (!vue.username) {
                bootbox.alert(strProblemIconHTML + 'Please login first.')
                return
            }

            const strDeleteFilename = vue.currentFilename
            if (strDeleteFilename === '') {
                bootbox.alert(strProblemIconHTML + 'No current item to delete.')
                return
            }

            const oAPIPostRequest = new XMLHttpRequest()
            oAPIPostRequest.onreadystatechange = (): void => {
                if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
                    let objResponse: any
                    if (oAPIPostRequest.responseText) {
                        objResponse = JSON.parse(oAPIPostRequest.responseText)
                    } else {
                        objResponse = null
                    }

                    if (
                        objResponse &&
                        (objResponse.productionFileExists || objResponse.draftFileExists || objResponse.suggestions)
                    ) {
                        const checkBoxes: { text: string; value: string }[] = []
                        if (objResponse.productionFileExists) {
                            checkBoxes.push({ text: 'Production file', value: 'p' })
                        }
                        if (objResponse.draftFileExists) {
                            checkBoxes.push({ text: 'Draft file', value: 'd' })
                        }
                        for (const suggestionFile of objResponse.suggestions) {
                            checkBoxes.push({
                                text: 'Suggestion by ' + suggestionFile.email,
                                value: suggestionFile.sugfilename,
                            })
                        }

                        bootbox.prompt({
                            title: 'Select files to delete.',
                            inputType: 'checkbox',
                            inputOptions: checkBoxes,
                            callback(result: any): void {
                                if (!result || !result.length) {
                                    return
                                }
                                let deleteRequest = ''
                                console.log(result)
                                for (const selectedCheckboxes of result) {
                                    deleteRequest = ''
                                    if (selectedCheckboxes === 'p' || selectedCheckboxes === 'd') {
                                        if (selectedCheckboxes === 'p') {
                                            deleteRequest =
                                                'Filename=' + encodeURIComponent(strDeleteFilename) + '&DeleteType=p'
                                        }
                                        if (selectedCheckboxes === 'd') {
                                            deleteRequest =
                                                'Filename=' + encodeURIComponent(strDeleteFilename) + '&DeleteType=d'
                                        }
                                    } else if (selectedCheckboxes) {
                                        deleteRequest =
                                            'Filename=' + encodeURIComponent(selectedCheckboxes) + '&DeleteType=s'
                                    }

                                    const oAPIDeletePostRequest = new XMLHttpRequest()
                                    oAPIDeletePostRequest.onreadystatechange = (): void => {
                                        if (oAPIDeletePostRequest.readyState === XMLHttpRequest.DONE) {
                                            let objDeleteResponse: any
                                            if (oAPIDeletePostRequest.responseText) {
                                                objDeleteResponse = JSON.parse(oAPIDeletePostRequest.responseText)
                                            } else {
                                                objDeleteResponse = null
                                            }

                                            if (oAPIDeletePostRequest.responseText === '') {
                                                if (typeof history.pushState !== 'undefined') {
                                                    window.history.pushState('', '', '?')
                                                    vue.isDraft = false
                                                }
                                                showToastMessage('File(s) deleted.', 2500)
                                            } else if (oAPIDeletePostRequest.responseText === 'login') {
                                                bootbox.alert(strProblemIconHTML + 'Please login first.')
                                                setLoginState(false)
                                                return
                                            } else if (
                                                objDeleteResponse &&
                                                objDeleteResponse.Error &&
                                                objDeleteResponse.Error.indexOf('Firebase ID token has expired') !== -1
                                            ) {
                                                bootbox.alert(strProblemIconHTML + 'Please login first.')
                                                setLoginState(false)
                                                return
                                            } else {
                                                bootbox.alert(
                                                    strProblemIconHTML +
                                                        'Unable to delete ' +
                                                        vue.currentFilename +
                                                        '.',
                                                )
                                            }
                                        }
                                    }
                                    oAPIDeletePostRequest.open('POST', strAPI)
                                    oAPIDeletePostRequest.setRequestHeader(
                                        'Content-type',
                                        'application/x-www-form-urlencoded',
                                    )
                                    oAPIDeletePostRequest.send('Action=delete&' + deleteRequest)
                                }
                            },
                        })
                    }
                }
            }

            oAPIPostRequest.open('POST', strAPI)
            oAPIPostRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
            oAPIPostRequest.send('Action=delete-list&' + 'Filename=' + encodeURIComponent(vue.currentFilename))
        },
        saveFilePost(publishFile = false): void {
            const oAPIPostRequest = new XMLHttpRequest()
            oAPIPostRequest.onreadystatechange = (): void => {
                if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
                    let objResponse: any
                    if (oAPIPostRequest.responseText) {
                        objResponse = JSON.parse(oAPIPostRequest.responseText)
                    } else {
                        objResponse = null
                    }
                    if (objResponse && objResponse.Error && objResponse.Error === 'Missing admin permission.') {
                        showToastMessage(strProblemIconHTML + 'Error: ' + objResponse.Error, 2500)
                    } else if (objResponse && objResponse.Error) {
                        showToastMessage(strProblemIconHTML + 'Error: ' + objResponse.Error, 2500)
                        setLoginState(false)
                        return
                    } else {
                        showToastMessage(
                            '<i class="fa fa-check text-success" aria-hidden="true"></i> File <b>' +
                                vue.currentFilename +
                                '</b> saved.',
                            2500,
                        )
                        vue.currentFileOriginalText = vue.simplemde.value()
                        if (typeof history.pushState !== 'undefined') {
                            window.history.pushState('', '', '?f=' + vue.currentFilename)
                        }
                        if (!publishFile) {
                            vue.isDraft = true
                        } else {
                            vue.isDraft = false
                        }
                    }
                }
            }
            let action = 'save-draft'
            if (publishFile) {
                action = 'publish'
            }
            oAPIPostRequest.open('POST', strAPI)
            oAPIPostRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
            oAPIPostRequest.send(
                'Action=' +
                    action +
                    '&' +
                    'Filename=' +
                    encodeURIComponent(vue.currentFilename) +
                    '&' +
                    'FileData=' +
                    encodeURIComponent(vue.simplemde.value()),
            )
        },
        publishFile(): void {
            vue.saveDraftFile(null, true)
        },
        saveDraftFile(event: any, publishFile: any): void {
            vue.suggestionEmail = ''
            if (!vue.username) {
                vue.username = getCookie('user')
            }
            vue.admin = getCookie('admin') === 'Y'
            if (!vue.username) {
                bootbox.alert(strProblemIconHTML + 'Please login first.')
                return
            }

            if (vue.currentFilename === '') {
                bootbox.alert(strProblemIconHTML + 'Please enter a name for this file.')
                return
            }

            if (!vue.currentFileOriginalText) {
                vue.saveFilePost(publishFile)
            } else {
                vue.getFileText(
                    vue.currentFilename,
                    '',
                    (response: {
                        draft: boolean
                        markdown: string
                        suggestionFile: string
                        productionFileData: string
                    }): void => {
                        const strFilesystemText = response.markdown

                        if (strFilesystemText === vue.currentFileOriginalText) {
                            vue.saveFilePost(publishFile)
                        } else {
                            let color = ''
                            let span = null
                            const diff = Diff.diffWords(vue.currentFileOriginalText, strFilesystemText)
                            const div = document.createElement('div')

                            diff.forEach(
                                (part: { added: boolean; removed: boolean; value: string }): void => {
                                    // green for additions, red for deletions
                                    // grey for common parts
                                    color = part.added ? 'green' : part.removed ? 'red' : 'grey'
                                    span = document.createElement('span')
                                    span.style.color = color
                                    span.appendChild(document.createTextNode(part.value))
                                    div.appendChild(span)
                                },
                            )

                            bootbox.confirm({
                                message:
                                    'The file <b>' +
                                    vue.currentFilename +
                                    `</b> has changed since you opened it.<br>
                <span style="color: red;">
                Overwrite with your changes?
                </span>
                <br>
                <br>
                Someone made the following changes:<br>
                <pre>` +
                                    div.innerHTML +
                                    '</pre>',
                                size: 'large',
                                buttons: {
                                    confirm: {
                                        label: 'Save',
                                        className: 'btn-primary',
                                    },
                                    cancel: {
                                        label: 'Cancel',
                                        className: 'btn-primary',
                                    },
                                },
                                callback(result: boolean): void {
                                    if (result) {
                                        vue.currentFileOriginalText = strFilesystemText
                                        vue.saveFilePost(publishFile)
                                    }
                                },
                            })
                        }
                    },
                )
            }
        },
        encryptCheck(): void {
            if (vue.simplemde.value() === 0) {
                bootbox.alert(strProblemIconHTML + 'No text to encrypt.')
                return
            }

            if (!strSavedEncryptionKey) {
                bootbox.prompt({
                    title: 'Enter an encryption password.',
                    inputType: 'password',
                    callback(result: string): void {
                        if (result) {
                            strSavedEncryptionKey = result
                            encrypt()
                        }
                        return
                    },
                })
            } else {
                bootbox.confirm(
                    'Use last encryption password?',
                    (result: any): void => {
                        if (!result) {
                            strSavedEncryptionKey = ''
                            vue.encryptCheck()
                            return
                        } else {
                            encrypt()
                        }
                    },
                )
            }
        },
        decryptCheck(): void {
            if (vue.simplemde.value() === 0) {
                bootbox.alert(strProblemIconHTML + 'No text to decrypt.')
                return
            }

            if (vue.simplemde.value().indexOf('ENCAES256') !== 0) {
                bootbox.alert(strProblemIconHTML + 'Text is not encrypted.')
                return
            }

            if (!strSavedEncryptionKey) {
                bootbox.prompt({
                    title: 'Enter an encryption password.',
                    inputType: 'password',
                    callback(result: string): void {
                        if (result) {
                            strSavedEncryptionKey = result
                            try {
                                decrypt()
                            } catch (err) {
                                bootbox.alert(strProblemIconHTML + 'Incorrect password for decrypting.')
                            }
                        }
                        return
                    },
                })
            } else {
                bootbox.confirm(
                    'Use last encryption password?',
                    (result: any): void => {
                        if (!result) {
                            strSavedEncryptionKey = ''
                            vue.decryptCheck()
                            return
                        } else {
                            try {
                                decrypt()
                            } catch (err) {
                                bootbox.alert(strProblemIconHTML + 'Incorrect password for decrypting.')
                            }
                        }
                    },
                )
            }
        },
        suggestChange(): void {
            if (!vue.username) {
                vue.username = getCookie('user')
            }
            vue.admin = getCookie('admin') === 'Y'
            if (!vue.username) {
                bootbox.alert(strProblemIconHTML + 'Please login first.')
                return
            }

            if (vue.currentFilename === '') {
                bootbox.alert(strProblemIconHTML + 'Please open a file before suggesting a change.')
                return
            }
            bootbox.dialog({
                title: 'Suggest a Change',
                message: '<p>Save the current text as a suggested change for this file?</p>',
                size: 'large',
                onEscape: true,
                backdrop: true,
                buttons: {
                    save: {
                        label: 'Save as suggestion.',
                        className: 'btn-primary',
                        callback: (): void => {
                            const oAPIPostRequest = new XMLHttpRequest()

                            oAPIPostRequest.onreadystatechange = (): void => {
                                if (oAPIPostRequest.readyState === XMLHttpRequest.DONE) {
                                    try {
                                        let oResponse: any
                                        if (oAPIPostRequest.responseText) {
                                            oResponse = JSON.parse(oAPIPostRequest.responseText)
                                        }

                                        if (oAPIPostRequest.responseText === '') {
                                            bootbox.alert('Saved suggested change for ' + vue.currentFilename + '.')
                                        } else if (
                                            oResponse &&
                                            oResponse.Error &&
                                            oResponse.Error.indexOf('Firebase ID token has expired') !== -1
                                        ) {
                                            bootbox.alert(strProblemIconHTML + 'Please login first.')
                                            setLoginState(false)
                                            return
                                        } else {
                                            bootbox.alert(
                                                strProblemIconHTML +
                                                    'Unable to save suggested change for ' +
                                                    vue.currentFilename +
                                                    '.',
                                            )
                                        }
                                    } catch (err) {
                                        bootbox.alert(strProblemIconHTML + 'Error: saving.')
                                    }
                                }
                            }
                            oAPIPostRequest.open('POST', strAPI)
                            oAPIPostRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
                            oAPIPostRequest.send(
                                'Action=suggest&' +
                                    'Filename=' +
                                    encodeURIComponent(vue.currentFilename) +
                                    '&' +
                                    'FileData=' +
                                    encodeURIComponent(vue.simplemde.value()),
                            )
                        },
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-primary',
                        callback: (): void => {
                            return
                        },
                    },
                },
            })
        },
    },
})

vue.pageLoded()
