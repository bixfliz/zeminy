var zeminySettings = `{
    "PageTitle": "Zeminy",
    "apiUrl": "/api/",
    "FooterHtml": "Zeminy Markdown Site",
    "EnableEncryption": true,
    "AlwaysShowMenu": false
  }`