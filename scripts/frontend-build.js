var shell = require('../backend/node_modules/shelljs');

shell.cd('../frontend');
shell.exec('../backend/node_modules/.bin/eslint -c .eslintrc.js --fix zeminy.ts');
shell.rm('-rf', 'dist');
shell.mkdir('dist')
shell.exec('../backend/node_modules/.bin/tsc -p tsconfig.json');
shell.cp('*.js', 'dist/');
shell.cp('manifest.json', 'dist/');
shell.cp('*.html', 'dist/');
shell.cp('-r', 'favicon', 'dist/');
shell.cp('-r', 'settings', 'dist/');
console.log('done')