maj=0
min=0
rev=8

cd ../backend
npm run frontend-build
cd ..
rm -rf docker/data/*
rm -rf docker/frontend/*
mkdir docker/frontend/dist
cp -r frontend/dist/* docker/frontend/dist/

rm -rf docker/backend/*
cp -r backend/*.json docker/backend/
cp -r backend/settings/settings-prod.toml docker/backend/
#cp -r backend/settings/settings.toml docker/backend/
cp -r backend/src docker/backend/
cp -r backend/settings docker/backend/

cd docker

docker build . -t zeminy:$maj.$min.$rev -t zeminy:latest
docker container stop zeminy
docker container rm zeminy
docker run --name zeminy -d -p 3000:3000 zeminy:$maj.$min.$rev
cd ..
docker save zeminy:$maj.$min.$rev > zeminy-$maj-$min-$rev.tar
7z a zeminy-$maj-$min-$rev.7z zeminy-$maj-$min-$rev.tar
rm zeminy-$maj-$min-$rev.tar
rm /home/jason/Documents/code/bixfliz-sync/bixfliz-docker/swarm/images/zeminy-*.7z
mv zeminy-$maj-$min-$rev.7z /home/jason/Documents/code/bixfliz-sync/bixfliz-docker/swarm/images